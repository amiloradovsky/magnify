
LDFLAGS += -lX11
CFLAGS  += -O2 -Wall
#CFLAGS  += -g -O0 -Wall  # debug version

magnify : main.c vector.c vector.h
	${CC} ${LDFLAGS} ${CFLAGS} -o magnify main.c vector.c

all : magnify

prefix = /usr
bindir = $(prefix)/bin

INSTALL = install
INSTALL_STRIP = -s
INSTALL_DIR = $(INSTALL) -d -m 0755
INSTALL_PROGRAM = $(INSTALL) -m 0755 $(INSTALL_STRIP)

install : magnify
	$(INSTALL_DIR) $(DESTDIR)$(bindir)
	$(INSTALL_PROGRAM) magnify $(DESTDIR)$(bindir)/

uninstall :
	rm -f $(DESTDIR)$(bindir)/magnify

clean :
	rm -f magnify
