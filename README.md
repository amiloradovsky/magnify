Magnifiers a screen region by an integer positive factor and draws the result on a window.
Useful as an accessibility tool, which works with every X Window System based GUI (depends only on `libX11`);
or as an assistant for graphic designers, who need to select individual pixels.

- No auxiliary transformations are done to the magnified image (crosshairs, color change etc.).
- No keyboard control possible (all the parameters must be given as the command-line arguments).

Usage:

    magnify [-w <width>] [-h <height>] [-m <factor>] [-r <rate>]

All the parameters must be positive integers.

- `width` and `height` are given in pixels; except for the value 0, in which case,
  width is set to the entire screen width, and height is set to 1/20 of it's height.
  This is the default value (0, 0), to work as an accessibility tool for text reading.
  (1/20 = 0.05 was chosen because of the so-called "rule of 5%"; a perception phenomenon)
- `factor` is set to 2 by default, since it's the least non-trivial value, which makes sense.
- `rate` is set to 25 by default, since it's the maximum value which a human can recognize;
  one might want do reduce it, to achieve a better performance on a slow/old machines.

For graphic design purposes, assuming the screen resolution is `1920x1080`,
one might, say, type:

    magnify -w `expr 1920 / 4` -h `expr 1080 / 4` -m 16
