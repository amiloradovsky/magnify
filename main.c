#include <stdlib.h>
#include <string.h>
#include <stdio.h>
#include <err.h>
#include <sysexits.h>

#include <signal.h>
#include <sys/time.h>
#include <unistd.h>

#include <X11/Xutil.h>

#include "vector.h"

#define DEF_H_PART 20  /* 0.05 (a la 2 sigma) */
#define DEF_RATE   25  /* highest perceivable */
#define DEF_FACTOR  2  /* lowest sensible */
#define DPY_W(dpy) DisplayWidth(dpy, DefaultScreen(dpy))
#define DPY_H(dpy) DisplayHeight(dpy, DefaultScreen(dpy))

u_int mf ;    /* magnification coefficient */
u_int rate ;  /* refresh per second */
Display *dpy;
Window win;

void
check_extent (
	u_int given  , char * name_of_given,
	u_int maximum, char * name_of_maximum )
{
	if ( given > maximum )
		err (EX_USAGE,
		     "%s is larger than %s", name_of_given, name_of_maximum);
}

void
check_geometry ( u_int_pair_t size )
{
	check_extent ( size.u0, "Width of the window",
	  DPY_W ( dpy ), "of the screen" );
	check_extent ( size.u1, "Height of the window",
	  DPY_H ( dpy ), "of the screen" );
}

void
draw_rectangle (
	XImage * buffer,
	u_long pixel,
	u_int_pair_t rwin,
	u_int u, u_int v )
{
	u_int i, j ;

	for ( i = 0 ; i != mf ; i ++ )
		if ( u + i != rwin.u0 )
			for ( j = 0 ; j != mf ; j ++ )
				if ( v + j != rwin.u1 )
					XPutPixel ( buffer, u + i, v + j, pixel );
				else break;
		else break;
}

/* 0 <= image size, pointer position <= screen size */
XImage *
capture ( u_int_pair_t position, u_int_pair_t size )
{
	const u_int_pair_t screen_size = { DPY_W ( dpy ), DPY_H ( dpy ) };
	u_int_pair_t edge_a, edge_b, origin ;

	/* Preventing of capturing out of the screen */
	edge_a = v2_floor ( size, 2 );
	edge_b = v2_sub ( screen_size, v2_sub ( size, edge_a ) );
	position = u2_fit_range ( edge_a, position, edge_b );
	origin = v2_sub ( position, edge_a );

	return XGetImage ( dpy, DefaultRootWindow ( dpy ),
	  origin.u0,origin.u1 , size.u0,size.u1 , AllPlanes, ZPixmap );
}

/* "full" is used for redrawing on exposure */
void
update ( u_int_pair_t position, u_int_pair_t size, Bool full )
{
	static u_int_pair_t rimg = { 0, 0 };
	static u_int_pair_t rwin = { 0, 0 };
	static XImage * buffer = NULL ;

	/* If geometry of the window was changed */
	if ( u2_ne ( size, rwin ) ) {
		check_geometry ( size );
		rwin = size ;
		rimg = v2_ceiling ( rwin, mf );
		full = True;

		if ( buffer ) XDestroyImage ( buffer );
		/* seems like libX11 offers no easier way to create an image */
		buffer = XGetImage ( dpy, DefaultRootWindow ( dpy ),
			0,0 , size.u0,size.u1 , AllPlanes, ZPixmap );
	}

	{
	static XImage * image0 = NULL ;
	XImage * image ;
	u_int x,y , u,v ;
	Bool changed ;

	image = capture ( position, rimg );

	/* To avoid trailing effects we can write first to a pixmap
	 * and then put it on the window */
	changed = False;
	for ( x = 0, u = 0 ; x != rimg.u0 ; x++, u += mf )
		for ( y = 0, v = 0 ; y != rimg.u1 ; y++, v += mf ) {
			u_long pixel;

			pixel = XGetPixel (image, x, y);
			if ( full || pixel != XGetPixel ( image0, x, y ) ) {
				draw_rectangle ( buffer, pixel, rwin, u, v );
				changed = True;
			}
		}
	if ( changed )
	  {
	  static GC gc = NULL ;

	  if ( gc == NULL ) gc = XCreateGC ( dpy, win, 0, NULL );
	  /* no values in the GC are specified */
	  XPutImage ( dpy, win, gc, buffer, 0,0 , 0,0 , rwin.u0,rwin.u1 );
	  }

	if ( image0 ) XDestroyImage ( image0 );
	image0 = image ;
	}
}

void
handler ()
{
	u_int_pair_t position ;
	u_int_pair_t size ;
	XEvent ev;
	Bool full;

	full = False;
	while ( XCheckWindowEvent ( dpy, win, ExposureMask, &ev ) == True )
		full = True;

	{
	unsigned int _ ;
	Window _w ;

	XGetGeometry ( dpy, win, &_w, ( int * ) &_,( int * ) &_ ,
		& ( size.u0 ),& ( size.u1 ) , &_, &_ );

	XQueryPointer ( dpy, DefaultRootWindow ( dpy ), &_w, &_w,
		( int * ) & ( position.u0 ),( int * ) & ( position.u1 ) ,
		( int * ) &_,( int * ) &_ , &_ );
	}

	update ( position, size, full );
}

void
term_handler ()
{
	warn ("exiting normally...");
	XUnmapWindow (dpy, win);
	XDestroyWindow (dpy, win);
	XCloseDisplay (dpy);
	/* Before exiting, you should call XCloseDisplay explicitly so that
	   any pending errors are reported as XCloseDisplay performs a final
	   XSync operation. (man page) */
        exit (EX_OK);
}

void
set_window_name (Display *display, Window window)
{
  // allocate it on the stack, because it's not
  // too large and the persistence isn't needed,
  // the name will be stored on the server anyway
  // (will set the WM_NAME property)
  char buffer[32];

  bzero (buffer, sizeof buffer);
  if (snprintf (buffer, sizeof buffer,
                "Magnify *%d@%d", mf, rate) <= 0)
    err (EX_SOFTWARE, "can't format window name");

  XStoreName (display, window, buffer);
}

Window
create_the_window (
	Display * display,
	u_int_pair_t size )
{
	Window window, root ;
	XSetWindowAttributes a;

	bzero ( &a, sizeof ( a ) );
	a.event_mask = ExposureMask ;
	root = DefaultRootWindow ( display );
	window = XCreateWindow ( display, root,
		0,0 , /* upper left edge of screen */
		size.u0,size.u1 ,
		0,               /* no border */
		CopyFromParent,  /* what is "depth"? */
		InputOutput,     /* yes, want to output */
		CopyFromParent,	 /* what is "visual"? */
		CWEventMask,     /* do not ignore the mask */
		&a );
	XMapWindow ( display, window );

	return window ;
}

int
main (int argc, char **argv)
{
	char c;
	u_int_pair_t size = { 0, 0 };
	struct itimerval it;

	mf = rate = 0 ;
	while ((c = getopt(argc, argv, "r:m:w:h:")) != -1)
		c == 'r' ? rate = atoi(optarg) :
		c == 'm' ? mf = atoi(optarg) :
		c == 'w' ? size.u0 = atoi(optarg) :
		c == 'h' ? size.u1 = atoi(optarg) :
		err (EX_USAGE,
		     "usage: %s [-w <1..>] [-h <1..>] [-m <2-8>] [-r <1-30>]\n",
		     *argv);

	if (!(dpy = XOpenDisplay(NULL)))
		err (EX_UNAVAILABLE, "Can't open display");

	if (!rate) rate = DEF_RATE;
	if (!mf) mf = DEF_FACTOR;
	if ( size.u0 == 0 ) size.u0 = DPY_W ( dpy );
	if ( size.u1 == 0 ) size.u1 = DPY_H ( dpy ) / DEF_H_PART ;
	check_geometry ( size );
	check_extent ( mf, "Magnification factor",
	  size.u0, "width of the window" );
	check_extent ( mf, "Magnification factor",
	  size.u1, "height of the window" );
	check_extent ( rate, "Refresh rate",
	  30, "human capabilities" );

	win = create_the_window ( dpy, size );
	set_window_name (dpy, win);

	signal ( SIGALRM, handler );
	signal ( SIGTERM, term_handler );
	signal ( SIGINT, term_handler );
	signal ( SIGHUP, term_handler );

	it.it_value.tv_sec     = 0 ;
	it.it_value.tv_usec    = 1 ;
	it.it_interval.tv_sec  = 0 ;
	it.it_interval.tv_usec = 1000000 / rate ;
	setitimer ( ITIMER_REAL, &it, 0 );

	while (pause ());

	err (EX_SOFTWARE, "exiting abnormally");
}
