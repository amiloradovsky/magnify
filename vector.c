#include "vector.h"

/*
 * comparison
 */

#define binary_relation(name, expr) bool_t \
name ( u_int_pair_t x, u_int_pair_t y ) { return ( expr ); }

binary_relation( u2_le, ( x.u0 <= y.u0 ) && ( x.u1 <= y.u1 ) )
binary_relation( u2_ge, ( x.u0 >= y.u0 ) && ( x.u1 >= y.u1 ) )
binary_relation( u2_eq, ( x.u0 == y.u0 ) && ( x.u1 == y.u1 ) )
binary_relation( u2_lt, ( x.u0 < y.u0 ) && ( x.u1 < y.u1 ) )
binary_relation( u2_gt, ( x.u0 > y.u0 ) && ( x.u1 > y.u1 ) )
binary_relation( u2_ne, ( x.u0 != y.u0 ) || ( x.u1 != y.u1 ) )
binary_relation( u2_ln, ( u2_le ( x, y ) ) && ( u2_ne ( x, y ) ) )
binary_relation( u2_gn, ( u2_ge ( x, y ) ) && ( u2_ne ( x, y ) ) )

/*
 * algebra
 */

u_int_pair_t
v2_add ( u_int_pair_t x, u_int_pair_t y )
	{
	u_int_pair_t z ;

	z.u0 = x.u0 + y.u0 ;
	z.u1 = x.u1 + y.u1 ;

	return z ;
	}

u_int_pair_t
v2_sub ( u_int_pair_t x, u_int_pair_t y )
	{
	u_int_pair_t z ;

	z.u0 = x.u0 - y.u0 ;
	z.u1 = x.u1 - y.u1 ;

	return z ;
	}

/* include both, for the symmetry */
u_int
ceil2 ( u_int x, u_int y )
	{
	u_int z ;

	z = x / y ;  /* then adjust the rounding */
	if ( ( z > 0 ) && ( x % y ) != 0 ) z ++ ;

	return z ;
	}

u_int
floor2 ( u_int x, u_int y )
	{
	u_int z ;

	z = x / y ;  /* then adjust the rounding */
	if ( ( z < 0 ) && ( x % y ) != 0 ) z ++ ;

	return z ;
	}

u_int_pair_t
v2_ceiling ( u_int_pair_t x, u_int k )
	{
	u_int_pair_t z ;

	z.u0 = ceil2 ( x.u0, k );
	z.u1 = ceil2 ( x.u1, k );

	return z ;
	}

u_int_pair_t
v2_floor ( u_int_pair_t x, u_int k )
	{
	u_int_pair_t z ;

	z.u0 = floor2 ( x.u0, k );
	z.u1 = floor2 ( x.u1, k );

	return z ;
	}

/* a <= b */
u_int
fit_range ( u_int a, u_int x, u_int b )
	{
	if ( a > x ) return a ;
	if ( x > b ) return b ;
	return x ;
	}

/* a <= b */
u_int_pair_t
u2_fit_range ( u_int_pair_t a, u_int_pair_t x, u_int_pair_t b )
	{
	u_int_pair_t z ;

	z.u0 = fit_range ( a.u0, x.u0, b.u0 );
	z.u1 = fit_range ( a.u1, x.u1, b.u1 );

	return z ;
	}

