
/*
 * Minimum set of 2-dimensional unsigned integer vector operations
 */

struct u_int_pair {
	unsigned int u0, u1 ;
};

typedef struct u_int_pair u_int_pair_t ;
typedef int bool_t ;  /* for the predicates */
typedef unsigned int u_int ;


bool_t u2_ne ( u_int_pair_t, u_int_pair_t );


u_int_pair_t
v2_add ( u_int_pair_t, u_int_pair_t );

u_int_pair_t
v2_sub ( u_int_pair_t, u_int_pair_t );

u_int_pair_t
v2_floor ( u_int_pair_t, u_int );

u_int_pair_t
v2_ceiling ( u_int_pair_t, u_int );

u_int_pair_t
u2_fit_range ( u_int_pair_t, u_int_pair_t, u_int_pair_t );

